CREATE DATABASE crm;

USE crm;

CREATE TABLE clients (
	id INT AUTO_INCREMENT,
	nome VARCHAR(120) NOT NULL,
	email VARCHAR(120) NOT NULL,
	cpf VARCHAR(11) NOT NULL,
	PRIMARY KEY(id)
);

INSERT INTO clients(nome,email,cpf) VALUES ('rogerio', 'rogerio@raccoon.ag', '11908242647');