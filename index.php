<?php
	
	require_once("clients.php");

	$uri = $_SERVER['REQUEST_URI'];
	$method = $_SERVER['REQUEST_METHOD'];

	if($uri == '/api/clients' && $method == 'GET') {
		get_all_clients();
	} else if($uri == '/api/clients' && $method == 'POST') {
		insert_client();
	}

?>