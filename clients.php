<?php

$conn = new PDO("mysql:host=localhost;dbname=crm", "root", "root");

function get_all_clients() {

	global $conn;

	// Executar um SQL
	$clients = $conn->prepare("SELECT * FROM clients");
	$clients->execute();

	// Escrever os resultados
	$data = array();
	foreach($clients->fetchAll() as $row):
		$data[] = array(
			'nome' => $row['nome'],
			'email' => $row['email'],
			'cpf' => $row['cpf']
		);
	endforeach;

	echo(json_encode($data));
}

function insert_client() {

	global $conn;

	$client = file_get_contents('php://input');
	$client = json_decode($client);

	if(!isset($client->nome) || !isset($client->email) || !isset($client->cpf)) {
		$response = array(
			'error' => true,
			'message' => 'fields is missing'
		);
		echo(json_encode($response));
		http_response_code(400);
		return 0;
	}

	$query = $conn->prepare("INSERT INTO clients(nome,email,cpf) VALUES('".$client->nome."','".$client->email."','".$client->cpf."')");
	$query->execute();

	$client->id = $conn->lastInsertId();

	echo(json_encode($client));

}